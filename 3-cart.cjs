const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]




// Q1. Find all the items with price more than $65.

let items = products.reduce((accumulater,current) => {
  if(Object.entries(current)[1].price.split('$')>65){
    accumulater[current] = current;
  }
  return accumulater;
},{});
console.log(items)

// Q2. Find all the items where quantity ordered is more than 1.
/*let quantityOrdered = products.reduce((accumulater,currentValue) => {

})*/

// Q.3 Get all items which are mentioned as fragile.
// Q.4 Find the least and the most expensive item for a single quantity.
// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

